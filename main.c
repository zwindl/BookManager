#include <stdio.h>
#include "new.h"

int main(int argc, char const *argv[]) {
	student stu;
	dynamic_array *array = malloc(sizeof(dynamic_array)); 
	array = new_list();
	list_operator op = new_list_operator();

	stu.name = "abc";
	stu.age = 12;
	stu.sex = "male";
	op.add(array, stu);

	stu.name = "cde";
	stu.age = 14;
	stu.sex = "male";
	op.add(array, stu);

	stu.name = "dkfjlsd";
	stu.age = 15;
	stu.sex = "male";
	op.add(array, stu);

	stu.name = "adkfjd";
	stu.age = 11;
	stu.sex = "male";
	op.add(array, stu);

	stu.name = "kllkk";
	stu.age = 12;
	stu.sex = "male";
	op.add(array, stu);

	for(int i=0; i<array->length; i++) {
		printf("name=%s\nsex=%s\nage=%d\n\n", array->items[i]->name, array->items[i]->sex,array->items[i]->age);
	}

	//测试列表清除功能
	op.clear(array);
	printf("cleared\n");

	//此时系统已经将 array 所占用的页帧标记为 free 了，输出结果会随机出现数据丢失
	for(int i=0; i<array->length; i++) {
		printf("name=%s\nsex=%s\nage=%d\n\n", array->items[i]->name, array->items[i]->sex,array->items[i]->age);
	}
	free(array);
	
	return 0;
}
