#include <stdio.h>
#include <stdlib.h>

#ifndef		_student
#define		_student
//学生信息
typedef struct student {
	char *name;
	char *sex;
	int age;
} student;

//学生信息动态数组
typedef struct dynamic_array {
	//student* items[x]
	//指针退化
	student** items;
	int length;
	/* data */
} dynamic_array;

dynamic_array * new_list() {
	dynamic_array *list = (dynamic_array*)calloc(1, sizeof(dynamic_array));
	//list -> items[0] = (student*)calloc(1, sizeof(student));
	list -> items = (student**)calloc(1, sizeof(student**));

	list -> length = 0;
	return list;
}

/*
 * 实现功能：动态地为 dynamic_array 添加项目
 * 用到的函数有 realloc, calloc 等
 */

typedef struct list_operator {
	void (*add)(dynamic_array *, student);
	void (*clear)(dynamic_array *);
}list_operator;

//添加操作
void add(dynamic_array *list, student stu) {
	student * item = (student*)malloc( sizeof(stu));
	*item = stu;

	list->length ++;

//	if(list -> length) {	//零个元素
//		list -> items[0] = item;
//	} else {
	list->items = (student**)realloc(list->items, sizeof(student*)*list->length);
	list->items[list->length - 1] = item;
//	}
}

//清除操作
void clear(dynamic_array *list) {
	for(int i=0; i<list->length; i++)
		free(list -> items[i]);
}

//初始化一个 list_operator
list_operator new_list_operator() {
	list_operator operator = {
		add,
		clear,
	};
	return operator;
}

#endif
